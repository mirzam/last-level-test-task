﻿using System;
using Unity.Collections;

namespace PathFinding.JobsPathFinder
{
	public struct NativeBinaryHeap<T> : IDisposable where T : unmanaged, IComparable<T>, IEquatable<T>
	{
		private NativeArray<T> _items;
		private NativeHashMap<T, int> _itemIndices;

		public NativeBinaryHeap (int maxHeapSize, Allocator allocator) {
			_items = new NativeArray<T>(maxHeapSize, allocator, NativeArrayOptions.UninitializedMemory);
			_itemIndices = new NativeHashMap<T, int>(128, allocator);
			Count = 0;
			Capacity = maxHeapSize;
		}

		public void Add (T item) {
			UpdateHeapItem(item, Count);
			SortUp(item);
			Count++;
		}

		public T RemoveFirst () {
			var firstItem = _items[0];
			Count--;
			var item = _items[Count];
			UpdateHeapItem(item, 0);
			SortDown(item);

			return firstItem;
		}

		public T RemoveAt (int index) {
			var firstItem = _items[index];
			Count--;
			if (index == Count) {
				return firstItem;
			}

			var item = _items[Count];
			UpdateHeapItem(item, index);
			SortDown(item);

			return firstItem;
		}

		public int Count { get; private set; }

		public int Capacity { get; }

		public int IndexOf (T item) {
			return GetHeapIndex(item);
		}

		public bool Contains(T item)
		{
			return IndexOf(item)!=-1;
		}

		public T this[int i] => _items[i];

		private void SortDown (T item) {
			while (true) {
				var itemIndex = GetHeapIndex(item);
				var childIndexLeft = itemIndex * 2 + 1;
				var childIndexRight = itemIndex * 2 + 2;

				if (childIndexLeft < Count) {
					var swapIndex = childIndexLeft;

					if (childIndexRight < Count) {
						if (_items[childIndexLeft].CompareTo(_items[childIndexRight]) < 0) {
							swapIndex = childIndexRight;
						}
					}

					if (item.CompareTo(_items[swapIndex]) < 0) {
						Swap(item, _items[swapIndex]);
					} else {
						return;
					}

				} else {
					return;
				}

			}
		}

		private void SortUp (T item) {
			var parentIndex = (GetHeapIndex(item) - 1) / 2;

			while (true) {
				var parentItem = _items[parentIndex];
				if (item.CompareTo(parentItem) > 0) {
					Swap(item, parentItem);
				} else {
					break;
				}

				parentIndex = (GetHeapIndex(item) - 1) / 2;
			}
		}

		private void Swap (T itemA, T itemB) {
			var itemAIndex = GetHeapIndex(itemA);
			var itemBIndex = GetHeapIndex(itemB);

			UpdateHeapItem(itemB, itemAIndex);
			UpdateHeapItem(itemA, itemBIndex);
		}

		private void UpdateHeapItem (T item, int newIndex) {
			_items[newIndex] = item;
			_itemIndices[item] = newIndex;
		}

		private int GetHeapIndex (T item) {
			if (_itemIndices.TryGetValue(item, out var result)) {
				return result;
			}
			return -1;
		}

		public void Dispose () {
			_items.Dispose();
			_itemIndices.Dispose();
		}
	}
}