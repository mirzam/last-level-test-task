﻿using System;
using Unity.Mathematics;

namespace PathFinding.JobsPathFinder
{
    public struct PathNode : IComparable<PathNode>, IEquatable<PathNode>
    {
        public int2 Position;
        public int Index;
        public float G;
        public float H;
        public float F => G + H;
        public bool IsWalkable;
        public int CameFromNodeIndex;
        
        public int CompareTo(PathNode other)
        {
            return -F.CompareTo(other.F);
        }

        public bool Equals(PathNode other)
        {
            return Index == other.Index;
        }
        
        public override int GetHashCode () {
            return Index.GetHashCode();
        }
    }
}