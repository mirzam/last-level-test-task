﻿using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace PathFinding.JobsPathFinder
{
    [BurstCompile]
    public struct FindPathJob : IJob
    {
        [ReadOnly]
        public int2 StartPosition;
        [ReadOnly]
        public int2 TargetPosition;
        [ReadOnly]
        public int2 GridSize;
        
        public NativeArray<PathNode> PathNodes;
        
        [WriteOnly]
        public NativeList<int2> ResultPath;

        private const float DiagonalMovementCost = math.SQRT2;
        private const float StraightMovementCost = 1f;
        

        public void Execute()
        {
            if (!IsPositionInsideGrid(StartPosition) || !IsPositionInsideGrid(TargetPosition)) return;

            var startNodeIndex = PositionToIndex(StartPosition);
            var targetNodeIndex = PositionToIndex(TargetPosition);
            
            var openNodes = new NativeBinaryHeap<PathNode>(PathNodes.Length, Allocator.Temp);
            var closedNodes = new NativeHashMap<int, bool>(PathNodes.Length, Allocator.Temp);
            
            var startNode = PathNodes[startNodeIndex];
            ResetNode(ref startNode);
            openNodes.Add(startNode);

            var neighbourOffsets = GetNeighbourOffsets();
            
            while (openNodes.Count>0)
            {
                var currentNode = GetPathNodeWithMinF(ref openNodes);
                
                if (currentNode.Index == targetNodeIndex) {
                    var targetNode = PathNodes[targetNodeIndex];
                    CalculatePath(targetNode);
                    break;
                }

                closedNodes[currentNode.Index] = true;

                for (var i = 0; i < neighbourOffsets.Length; i++)
                {
                    var neighbourOffset = neighbourOffsets[i];
                    var neighbourPosition = neighbourOffset + currentNode.Position; 
                    if (!IsPositionInsideGrid(neighbourPosition))
                    {
                        continue;
                    }

                    var neighbourNodeIndex = PositionToIndex(neighbourPosition);
                    var neighbourNode = PathNodes[neighbourNodeIndex];
                    if (closedNodes.ContainsKey(neighbourNodeIndex) || !neighbourNode.IsWalkable)
                    {
                        continue;
                    }

                    var tentativeGCost = currentNode.G +
                                         GetDistanceBetweenPositions(currentNode.Position, neighbourPosition);

                    if (!openNodes.Contains(neighbourNode))
                    {
                        var h = GetDistanceBetweenPositions(neighbourNode.Position, TargetPosition);
                        UpdateNode(ref neighbourNode, tentativeGCost, h, currentNode.Index);
                        openNodes.Add(neighbourNode);
                    }
                    else if (tentativeGCost < neighbourNode.G)
                    {
                        UpdateNode(ref neighbourNode, tentativeGCost, neighbourNode.H, currentNode.Index);
                    }
                }
            }
            
            neighbourOffsets.Dispose();
            openNodes.Dispose();
            closedNodes.Dispose();
        }

        private NativeArray<int2> GetNeighbourOffsets()
        {
            var neighbourOffsetArray = new NativeArray<int2>(8, Allocator.Temp);
            neighbourOffsetArray[0] = new int2(-1, 0);
            neighbourOffsetArray[1] = new int2(+1, 0);
            neighbourOffsetArray[2] = new int2(0, +1);
            neighbourOffsetArray[3] = new int2(0, -1);
            neighbourOffsetArray[4] = new int2(-1, -1);
            neighbourOffsetArray[5] = new int2(-1, +1);
            neighbourOffsetArray[6] = new int2(+1, -1);
            neighbourOffsetArray[7] = new int2(+1, +1);
            return neighbourOffsetArray;
        }
        
        private int PositionToIndex(int2 position) {
            return position.x + position.y * GridSize.x;
        }
        
        private bool IsPositionInsideGrid(int2 position) {
            return
                position.x >= 0 && 
                position.y >= 0 &&
                position.x < GridSize.x &&
                position.y < GridSize.y;
        }

        private PathNode GetPathNodeWithMinF(ref NativeBinaryHeap<PathNode> openList)
        {
            var minFNodeIndex = openList.RemoveFirst().Index;
            return PathNodes[minFNodeIndex];
        }

        private void CalculatePath(PathNode endNode)
        {
            if (endNode.CameFromNodeIndex == -1) return;
            ResultPath.Add(endNode.Position);

            var currentNode = endNode;
            while (currentNode.CameFromNodeIndex != -1)
            {
                var cameFromNode = PathNodes[currentNode.CameFromNodeIndex];
                ResultPath.Add(cameFromNode.Position);
                currentNode = cameFromNode;
            }
        }

        private static float GetDistanceBetweenPositions(int2 from, int2 to)
        {
            var xDistance = math.abs(from.x - to.x);
            var yDistance = math.abs(from.y - to.y);
            return StraightMovementCost * (xDistance + yDistance) +
                   (DiagonalMovementCost - 2f* StraightMovementCost) * math.min(xDistance, yDistance);
        }

        private void ResetNode(ref PathNode pathNode)
        {
            UpdateNode(ref pathNode, 0, GetDistanceBetweenPositions(pathNode.Position, TargetPosition), -1);
        }

        private void UpdateNode(ref PathNode pathNode, float g, float h, int cameFromNodeIndex)
        {
            pathNode.G = g;
            pathNode.H = h;
            pathNode.CameFromNodeIndex = cameFromNodeIndex;
            PathNodes[pathNode.Index] = pathNode;
        }
    }
}