﻿using System.Collections.Generic;
using System.Linq;
using Map;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace PathFinding.JobsPathFinder
{
    public class JobsPathFinder : IPathFinder
    {
        private IGridMap _gridMap;
        private NativeArray<PathNode> _pathNodes;
        public void Init(IGridMap gridMap)
        {
            _gridMap = gridMap;
            _pathNodes = CreatePathNodesArray();
            if (_gridMap!=null)
                _gridMap.CellIsWalkableChanged += OnCellIsWalkableChanged;
        }

        private void OnCellIsWalkableChanged(int x, int y, bool oldIsWalkable, bool newIsWalkable)
        {
            if (_gridMap.IsPositionInsideGrid(x, y))
            {
                var pathNode = _pathNodes[x + y * _gridMap.Width];
                pathNode.IsWalkable = newIsWalkable;
                _pathNodes[x + y * _gridMap.Width] = pathNode;
            }
        }

        public List<Vector2Int> FindPath(Vector2Int startPosition, Vector2Int targetPosition)
        {
            var path = new NativeList<int2>(Allocator.TempJob);
            if (!_pathNodes.IsCreated)
                _pathNodes = CreatePathNodesArray();
            
            var findPathJob = new FindPathJob()
            {
                StartPosition = new int2(startPosition.x, startPosition.y),
                TargetPosition = new int2(targetPosition.x, targetPosition.y),
                GridSize = new int2(_gridMap.Width, _gridMap.Height),
                PathNodes = _pathNodes,
                ResultPath = path
            };

            var handle = findPathJob.Schedule();
            // По идее, не очень красиво вызывать Complete в этом месте, т.к. мы блокируем основной поток,
            // но я оставил так, чтобы было проще замерять время выполнения. При реальном использовании следовало
            // бы добавлять задачи в планировщик в начале кадра и забирать результат выполнения в конце, например
            // в LateUpdate()
            handle.Complete();

            if (path.Length == 0)
            {
                path.Dispose();
                return null;
            }

            var result = new List<Vector2Int>();

            for (var i = 0; i < path.Length; i++)
            {
                result.Add(new Vector2Int(path[i].x, path[i].y));
            }
            
            path.Dispose();
            return result;
        }

        private NativeArray<PathNode> CreatePathNodesArray()
        {
            var pathNodes = new NativeArray<PathNode>(_gridMap.Height * _gridMap.Width, Allocator.Persistent);
            for (var x = 0; x < _gridMap.Width; x++)
            {
                for (var y = 0; y < _gridMap.Height; y++)
                {
                    var cell = _gridMap.GetCell(x, y);
                    var nodeIndex = x + y * _gridMap.Width;
                    pathNodes[nodeIndex] = new PathNode()
                    {
                        Position = new int2(x, y),
                        Index = nodeIndex,
                        IsWalkable = cell.IsWalkable,
                    };
                }
            }

            return pathNodes;
        }

        public void Dispose()
        {
            _pathNodes.Dispose();
            if (_gridMap!=null)
                _gridMap.CellIsWalkableChanged -= OnCellIsWalkableChanged;
        }
    }
}