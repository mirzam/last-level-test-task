﻿using System;
using System.Collections.Generic;
using Map;
using UnityEngine;

namespace PathFinding.SimplePathFinder
{
    public class SimplePathFinder : IPathFinder
    {
        private IGridMap _gridMap;
        private PathNode[,] _pathNodes;


        private static readonly float DiagonalMovementCost = Mathf.Sqrt(2);
        private static readonly float StraightMovementCost = 1f;


        public void Init(IGridMap gridMap)
        {
            _gridMap = gridMap ?? throw new ArgumentNullException();
            CreatePathNodes(gridMap);
        }

        public List<Vector2Int> FindPath(Vector2Int startPosition, Vector2Int targetPosition)
        {
            if (!_gridMap.IsPositionInsideGrid(startPosition.x, startPosition.y) ||
                !_gridMap.IsPositionInsideGrid(targetPosition.x, targetPosition.y))
                return null;
            var startNode = _pathNodes[startPosition.x, startPosition.y];
            var targetNode = _pathNodes[targetPosition.x, targetPosition.y];

            // Тут следовало бы использовать бинарную кучу, но я решил в этом варианте обойтись списком с сортированной вставкой.
            var openList = new List<PathNode>() {startNode};
            var closeSet = new HashSet<PathNode>();

            startNode.G = 0f;
            startNode.H = GetDistanceBetweenPositions(startNode.Position, targetPosition);
            startNode.CameFromNode = null;

            while (openList.Count > 0)
            {
                var currentNode = GetPathNodeWithMinF(openList);
                if (currentNode == targetNode)
                {
                    return CalculatePath(currentNode);
                }

                openList.Remove(currentNode);
                closeSet.Add(currentNode);
                foreach (var neighbour in GetNeighbourList(currentNode))
                {
                    if (closeSet.Contains(neighbour) || !neighbour.IsWalkable) continue;

                    var tentativeGCost = currentNode.G +
                                         GetDistanceBetweenPositions(currentNode.Position, neighbour.Position);

                    // Тут можно оптимизировать, добавлением еще одного HashSet для открытых вершин,
                    // т.к. Contains() у списка может быть долгим
                    if (!openList.Contains(neighbour))
                    {
                        neighbour.H = GetDistanceBetweenPositions(neighbour.Position, targetPosition);
                        neighbour.G = tentativeGCost;
                        neighbour.CameFromNode = currentNode;
                        SortedAddToList(openList, neighbour);
                    }
                    else if (tentativeGCost < neighbour.G)
                    {
                        neighbour.G = tentativeGCost;
                        neighbour.CameFromNode = currentNode;
                    }
                }
            }

            return null;
        }

        private void CreatePathNodes(IGridMap gridMap)
        {
            _pathNodes = new PathNode[gridMap.Width, gridMap.Height];
            for (var x = 0; x < gridMap.Width; x++)
            {
                for (var y = 0; y < gridMap.Height; y++)
                {
                    _pathNodes[x, y] = new PathNode(new Vector2Int(x, y), gridMap.GetCell(x, y));
                }
            }
        }

        private static float GetDistanceBetweenPositions(Vector2Int from, Vector2Int to)
        {
            var xDistance = Mathf.Abs(from.x - to.x);
            var yDistance = Mathf.Abs(from.y - to.y);
            return StraightMovementCost * (xDistance + yDistance) +
                   (DiagonalMovementCost - 2f * StraightMovementCost) * Mathf.Min(xDistance, yDistance);
        }

        private static PathNode GetPathNodeWithMinF(List<PathNode> list)
        {
            if (list.Count == 0) return null;
            var node = list[0];
            list.RemoveAt(0);
            return node;
        }

        private static void SortedAddToList(List<PathNode> list, PathNode node)
        {
            for (var i = 0; i < list.Count; ++i)
            {
                if (node.F < list[i].F)
                {
                    list.Insert(i, node);
                    return;
                }
            }

            list.Add(node);
        }

        private static List<Vector2Int> CalculatePath(PathNode endNode)
        {
            var path = new List<Vector2Int> {endNode.Position};
            var currentNode = endNode;
            while (currentNode.CameFromNode != null)
            {
                path.Add(currentNode.CameFromNode.Position);
                currentNode = currentNode.CameFromNode;
            }

            path.Reverse();
            return path;
        }

        private List<PathNode> GetNeighbourList(PathNode node)
        {
            var neighbours = new List<PathNode>();
            for (var xOffset = -1; xOffset <= 1; xOffset++)
            {
                for (var yOffset = -1; yOffset <= 1; yOffset++)
                {
                    var x = node.Position.x + xOffset;
                    var y = node.Position.y + yOffset;
                    if ((x == 0 && y == 0) || !_gridMap.IsPositionInsideGrid(x, y))
                        continue;
                    neighbours.Add(_pathNodes[x, y]);
                }
            }

            return neighbours;
        }

        public void Dispose()
        {
        }
    }
}