﻿using System;
using Map;
using UnityEngine;

namespace PathFinding.SimplePathFinder
{
    public class PathNode
    {
        public Vector2Int Position { get; }
        public float G;
        public float H;
        public float F => G + H;
        public bool IsWalkable => _cell.IsWalkable;
        public PathNode CameFromNode;

        private readonly ICell _cell;

        public PathNode(Vector2Int position, ICell cell)
        {
            _cell = cell ?? throw new ArgumentNullException();
            Position = position;
        }
    }
}