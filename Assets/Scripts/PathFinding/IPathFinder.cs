﻿using System;
using System.Collections.Generic;
using Map;
using UnityEngine;

namespace PathFinding
{
    public interface IPathFinder : IDisposable
    {
        void Init(IGridMap gridMap);
        List<Vector2Int> FindPath(Vector2Int startPosition, Vector2Int targetPosition);
    }
}