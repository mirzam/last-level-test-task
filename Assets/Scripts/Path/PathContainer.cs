﻿using System;
using System.Collections.Generic;
using Map;
using PathFinding;
using PathFinding.JobsPathFinder;
using PathFinding.SimplePathFinder;
using UnityEngine;
using UnityEngine.Serialization;

namespace Path
{
    public class PathContainer : MonoBehaviour
    {
        public delegate void PositionChangedEventHandler(Vector2Int position);
        public delegate void PathChangedEventHandler(List<Vector2Int> path);

        public event PositionChangedEventHandler StartPositionChanged;
        public event PositionChangedEventHandler TargetPositionChanged;
        public event PathChangedEventHandler PathChanged;

        [SerializeField] private GridMap gridMap;
        private IPathFinder _pathFinder;
        
        private Vector2Int _startPosition;
        private Vector2Int _targetPosition;
        private List<Vector2Int> _path;

        private void Start()
        {
            if (gridMap!=null)
            {
                SetPathFinder<JobsPathFinder>();
                gridMap.CellIsWalkableChanged+= GridMapOnCellIsWalkableChanged;
            }
        }

        private void OnDestroy()
        {
            if (gridMap!=null)
                gridMap.CellIsWalkableChanged-= GridMapOnCellIsWalkableChanged;
            _pathFinder?.Dispose();
        }

        private void GridMapOnCellIsWalkableChanged(int x, int y, bool oldiswalkable, bool newiswalkable)
        {
            if (oldiswalkable!=newiswalkable)
                UpdatePath();
        }

        public void SetPathFinder<T>() where T : IPathFinder, new()
        {
            if (_pathFinder is T) return;
            _pathFinder?.Dispose();
            
            _pathFinder = new T();
            _pathFinder.Init(gridMap);
            Debug.Log($"Pathfinder changed: {_pathFinder}");
        }

        public void SetStartPosition(Vector2Int startPosition)
        {
            if (startPosition==_targetPosition) return;
            _startPosition = startPosition;
            StartPositionChanged?.Invoke(startPosition);
            UpdatePath();
        }
        
        public void SetTargetPosition(Vector2Int targetPosition)
        {
            if (targetPosition==_startPosition) return;
            _targetPosition = targetPosition;
            TargetPositionChanged?.Invoke(targetPosition);
            UpdatePath();
        }

        private void SetPath(List<Vector2Int> path)
        {
            _path = path;
            PathChanged?.Invoke(path);
        }

        private void UpdatePath()
        {
            if (_pathFinder != null)
            {
                var startTime = Time.realtimeSinceStartup;
                var path = _pathFinder.FindPath(_startPosition, _targetPosition);
                if (path!=null)
                    Debug.Log($"Path solved in: {(Time.realtimeSinceStartup - startTime) * 1000f} ms");
                SetPath(path);
            }
        }
    }
}
