﻿using System;
using System.Collections.Generic;
using PathFinding.JobsPathFinder;
using PathFinding.SimplePathFinder;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;

namespace Path
{
    public class PathPresenter : MonoBehaviour
    {
        [SerializeField] private PathContainer pathContainer;
        [SerializeField] private Tilemap tilemap;
        [SerializeField] private TileBase startPositionTile;
        [SerializeField] private TileBase targetPositionTile;
        [SerializeField] private TileBase pathTile;
        
        [SerializeField] private int pathZ = 1;
        [SerializeField] private int markersZ = 2;

        private Vector2Int _currentStartPosition;
        private Vector2Int _currentTargetPosition;
        private List<Vector2Int> _currentPath;

        private void OnEnable()
        {
            if (pathContainer)
            {
                pathContainer.StartPositionChanged += OnStartPositionChanged;
                pathContainer.TargetPositionChanged += OnTargetPositionChanged;
                pathContainer.PathChanged += OnPathChanged;
            }
        }

        private void OnDisable()
        {
            if (pathContainer)
            {
                pathContainer.StartPositionChanged -= OnStartPositionChanged;
                pathContainer.TargetPositionChanged -= OnTargetPositionChanged;
                pathContainer.PathChanged -= OnPathChanged;
            }
        }

        private void Update()
        {
            HandleInput();
        }

        private void HandleInput()
        {
            if (!tilemap || !pathContainer) return;
            if (Input.GetKeyDown(KeyCode.S))
            {
                var clickedTilePosition = tilemap.MousePositionToTilePosition();
                pathContainer.SetStartPosition(new Vector2Int(clickedTilePosition.x, clickedTilePosition.y));
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                var clickedTilePosition = tilemap.MousePositionToTilePosition();
                pathContainer.SetTargetPosition(new Vector2Int(clickedTilePosition.x, clickedTilePosition.y));
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                pathContainer.SetPathFinder<SimplePathFinder>();
            }
            
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                pathContainer.SetPathFinder<JobsPathFinder>();
            }
        }

        private void OnPathChanged(List<Vector2Int> path)
        {
            if (tilemap)
            {
                if (_currentPath != null)
                {
                    foreach (var position in _currentPath)
                    {
                        tilemap.SetTile(new Vector3Int(position.x, position.y, pathZ), null);
                    }
                }

                if (path != null)
                    foreach (var position in path)
                    {
                        tilemap.SetTile(new Vector3Int(position.x, position.y, pathZ), pathTile);
                    }

                _currentPath = path;
            }
        }

        private void OnTargetPositionChanged(Vector2Int position)
        {
            if (tilemap)
            {
                tilemap.SetTile(new Vector3Int(_currentTargetPosition.x, _currentTargetPosition.y, markersZ), null);
                tilemap.SetTile(new Vector3Int(position.x, position.y, markersZ), targetPositionTile);
                _currentTargetPosition = position;
            }
        }

        private void OnStartPositionChanged(Vector2Int position)
        {
            if (tilemap)
            {
                tilemap.SetTile(new Vector3Int(_currentStartPosition.x, _currentStartPosition.y, markersZ), null);
                tilemap.SetTile(new Vector3Int(position.x, position.y, markersZ), startPositionTile);
                _currentStartPosition = position;
            }
        }
    }
}