﻿using UnityEngine;
using UnityEngine.Tilemaps;

public static class TileMapExtensions
{
    public static Vector3Int MousePositionToTilePosition(this Tilemap tilemap)
    {
        var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return tilemap.layoutGrid.WorldToCell(mouseWorldPos);
    }
}
