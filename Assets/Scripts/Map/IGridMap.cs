﻿namespace Map
{
    public interface IGridMap
    {
        event CellIsWalkableChangedEventHandler CellIsWalkableChanged;
        
        int Width { get; }
        int Height { get; }
        ICell GetCell(int x, int y);
    }
}