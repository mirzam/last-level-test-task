﻿namespace Map
{
    public interface ICell
    {
        bool IsWalkable { get; }
    }
}