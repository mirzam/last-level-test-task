﻿namespace Map
{
    public delegate void CellIsWalkableChangedEventHandler(int x, int y,  bool oldIsWalkable, bool newIsWalkable);
}