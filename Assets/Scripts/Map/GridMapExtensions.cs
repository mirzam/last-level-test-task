﻿namespace Map
{
    public static class GridMapExtensions
    {
        public static bool IsPositionInsideGrid(this IGridMap gridMap, int x, int y)
        {
            return (x < gridMap.Width && x >= 0 && y < gridMap.Height && y >= 0);
        }
    }
}