﻿using System;
using UnityEngine;

namespace Map
{
    public class LoadTextureToGridMap : MonoBehaviour
    {
        [SerializeField] private Texture2D _texture;
        [SerializeField] private GridMap _gridMap;
        [SerializeField, Range(0f,1f)] private float walkableThreshold = 0.1f;

        private void Start()
        {
            Load();
        }

        private void Load()
        {
            if (_texture && _gridMap)
            {
                if (_texture.width>_gridMap.Width || _texture.height>_gridMap.Height)
                    _gridMap.InitGrid(_texture.width, _texture.height);

                var pixels = _texture.GetPixels();
                for (var i = 0; i < pixels.Length; i++)
                {
                    var x = i % _texture.width;
                    var y = i / _texture.width;
                    var isWalkable = pixels[i].grayscale > walkableThreshold; 
                    _gridMap.SetCellWalkable(x,y, isWalkable);
                }
            }
        }
    }
}