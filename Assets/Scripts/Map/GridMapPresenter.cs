﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;

namespace Map
{
    public class GridMapPresenter : MonoBehaviour
    {
        [SerializeField] private GridMap gridMap;
        [SerializeField] private Tilemap tilemap;
        [SerializeField] private TileBase walkableTile;
        [SerializeField] private TileBase notWalkableTile;

        [SerializeField] private int tilesZ = 0;


        private void Start()
        {
            FillTileMap();
        }

        private void OnEnable()
        {
            if (gridMap)
                gridMap.CellIsWalkableChanged+= OnCellIsWalkableChanged;
        }

        private void OnDisable()
        {
            if (gridMap)
                gridMap.CellIsWalkableChanged -= OnCellIsWalkableChanged;
        }

        private void Update()
        {
            HandleMouseClicks();
        }

        private void OnCellIsWalkableChanged(int x, int y, bool oldIsWalkable, bool newIsWalkable)
        {
            UpdateTile(x,y,newIsWalkable);
        }

        private void FillTileMap()
        {
            if (gridMap && tilemap && walkableTile && notWalkableTile)
            {
                for (var x = 0; x < gridMap.Width; x++)
                {
                    for (var y = 0; y < gridMap.Height; y++)
                    {
                        UpdateTile(x, y, gridMap.GetCell(x, y).IsWalkable);
                    }
                }
            }
        }

        private void UpdateTile(int x, int y, bool isWalkable)
        {
            tilemap.SetTile(new Vector3Int(x, y, tilesZ), isWalkable? walkableTile : notWalkableTile);
        }

        private void HandleMouseClicks()
        {
            if (!tilemap || ! gridMap) return;
            if (Input.GetMouseButtonDown(0))
            {
                var clickedTilePosition = tilemap.MousePositionToTilePosition();
                gridMap.SetCellWalkable(clickedTilePosition.x, clickedTilePosition.y, false);
            }
            if (Input.GetMouseButtonDown(1))
            {
                var clickedTilePosition = tilemap.MousePositionToTilePosition();
                gridMap.SetCellWalkable(clickedTilePosition.x, clickedTilePosition.y, true);
            }
        }

    }
}