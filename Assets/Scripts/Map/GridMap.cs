﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Map
{
    public class GridMap : MonoBehaviour, IGridMap
    {
        private class Cell : ICell
        {
            public bool IsWalkable { get; private set; }

            public Cell(bool isWalkable)
            {
                IsWalkable = isWalkable;
            }

            public void SetIsWalkable(bool isWalkable)
            {
                IsWalkable = isWalkable;
            }
        }

        public event CellIsWalkableChangedEventHandler CellIsWalkableChanged;

        private const int DefaultWidth = 100;
        private const int DefaultHeight = 100;

        public int Width => _width;
        public int Height => _height;
        
        private Cell[,] _grid;
        private int _width = 0;
        private int _height = 0;

        private void Awake()
        {
            InitGrid(DefaultWidth, DefaultHeight);
        }

        public ICell GetCell(int x, int y)
        {
            if (x > _width || x<0 || y > _height || y<0) return null;
            return _grid?[x, y];
        }

        public void SetCellWalkable(int x, int y, bool isWalkable)
        {
            if (x > _width || x<0 || y > _height || y<0)
            { 
                Debug.LogError("Try to set cell from out of bounds", this);
            };
            if (_grid != null)
            {
                var cell = _grid[x, y];
                CellIsWalkableChanged?.Invoke(x, y, cell.IsWalkable, isWalkable);
                if (cell.IsWalkable != isWalkable)
                {
                    cell.SetIsWalkable(isWalkable);
                }
            }
        }

        public void InitGrid(int width, int height)
        {
            _width = Mathf.Max(width, 0);
            _height = Mathf.Max(height, 0);
            _grid = new Cell[width, height];
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    _grid[i, j] = new Cell(true);
                }
            }
        }
        
    }
}

